import 'package:controlmefpb/inicio.dart';
import 'package:controlmefpb/modulos/principal/paginaPrincipal.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;

class PaginaSesion extends StatefulWidget {
  const PaginaSesion({Key? key}) : super(key: key);

  @override
  _PaginaSesionState createState() => _PaginaSesionState();
}

class _PaginaSesionState extends State<PaginaSesion> {
  bool _inAsyncCall = false;
  int genderSel = 1;
  int selected = 2;
  int currentIndex = 3;
  bool ini = true;
  bool create = false;
  bool showPassword = true;
  bool checkBox = false;
  bool button = true;
  String value = "";
  final _formIni = GlobalKey<FormState>();
  TextEditingController usuario = TextEditingController();
  TextEditingController contrasena = TextEditingController();

  String message = "";
  String messagelog = "";

  setSelected(int val) {
    setState(() {
      selected = val;
    });
  }

  Future logPersonal() async {
    final response = await http.post(
        Uri.parse('http://172.20.250.14:3001/auth'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8'
        },
        body: jsonEncode(<String, String>{
          'usuario': '${usuario.value.text}',
          'contrasena': '${contrasena.value.text}'
        }));
    var dataUser = jsonDecode(response.body);
    print(dataUser);
    List<String> resp = dataUser.toString().split("nombres");
    messagelog = resp.toString();
    //messagelog = resp[resp.length - 1].replaceAll("}", "");
    print("meeeeeeeeeeeeeeeeeeeeeeeeeeeeeesssssssssssss$messagelog");
  }

  @override
  void initState() {
    usuario.clear();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          SingleChildScrollView(
              child: Column(
            children: <Widget>[
              SizedBox(
                height: 50,
              ),
              Container(
                  margin: EdgeInsets.only(bottom: 20),
                  child: Image.asset(
                    "assets/images/logo.png",
                    fit: BoxFit.cover,
                    //height: 50,
                    width: 300,
                  )),
              Container(
                padding: EdgeInsets.only(top: 40),
                decoration: BoxDecoration(
                    color: Color.fromARGB(255, 27, 87, 110),
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(100),
                      topLeft: Radius.circular(100),
                    )),
                child: Column(
                  children: [
                    Text(
                      "Bienvenido",
                      style: TextStyle(
                        color: Colors.white, fontSize: 20,
                        // fontWeight: FontWeight.bold
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      margin: EdgeInsets.all(25),
                      //width: MediaQuery.of(context).size.width/1.5,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.white.withOpacity(0.5),
                      ),
                      child: Column(
                        children: <Widget>[
                          Form(
                              key: _formIni,
                              child: Visibility(
                                  visible: ini,
                                  child: Column(
                                    children: <Widget>[
                                      Container(
                                          padding: EdgeInsets.only(
                                              left: 10, right: 10),
                                          margin: EdgeInsets.all(5),
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: TextFormField(
                                            validator: (m) {
                                              return (m == null || m.isEmpty)
                                                  ? 'Error!.Intenta de nuevo'
                                                  : null;
                                            },
                                            decoration: InputDecoration(
                                                errorStyle: TextStyle(
                                                    fontSize: 12,
                                                    fontStyle: FontStyle.italic,
                                                    color: Colors.red.shade900),
                                                hintText: "Ingresa tu usuario",
                                                hintStyle: TextStyle(
                                                    color: Colors.grey.shade600,
                                                    fontSize: 14),
                                                border: InputBorder.none),
                                            autofocus: false,
                                            controller: usuario,
                                            cursorColor: Colors.black,
                                            autocorrect: true,
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 14,
                                            ),
                                          )),
                                      Container(
                                        margin: EdgeInsets.only(
                                            left: 5, right: 5, bottom: 5),
                                        padding:
                                            EdgeInsets.only(left: 5, right: 10),
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        child: TextFormField(
                                          obscureText: showPassword,
                                          validator: (p) {
                                            return (p == null || p.isEmpty)
                                                ? 'Error!.Intenta de nuevo'
                                                : null;
                                            //return 'prosigue';
                                          },
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 14),
                                          autofocus: false,
                                          controller: contrasena,
                                          decoration: InputDecoration(
                                              errorStyle: TextStyle(
                                                  color: Colors.red.shade900,
                                                  fontSize: 12,
                                                  fontStyle: FontStyle.italic),
                                              border: InputBorder.none,
                                              hintText: "Ingresa tu contraseña",
                                              hintStyle: TextStyle(
                                                  color: Colors.grey.shade600,
                                                  fontSize: 14)),
                                          cursorColor: Colors.black,
                                        ),
                                      ),
                                      Container(
                                        child: Row(
                                          children: <Widget>[
                                            Checkbox(
                                                activeColor: Colors.white,
                                                checkColor: Colors.blue,
                                                value: checkBox,
                                                onChanged: (val) {
                                                  setState(() {
                                                    showPassword =
                                                        !showPassword;
                                                    checkBox = val!;
                                                  });
                                                }),
                                            Text(
                                              "Mostrar contraseña",
                                              style: TextStyle(
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.w600),
                                            )
                                          ],
                                        ),
                                      ),
                                      RaisedButton(
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(20)),
                                        splashColor:
                                            Colors.white.withOpacity(0.8),
                                        onPressed: () async {
                                          if (_formIni.currentState!
                                              .validate()) {
                                            setState(() {
                                              _inAsyncCall = true;
                                            });
                                            logPersonal();
                                            Timer(Duration(seconds: 3),
                                                () async {
                                              if (!messagelog.contains(
                                                  "Usuario o contraseña incorrectos o no tiene roles")) {
                                                _inAsyncCall = false;
                                                Navigator.of(context).push(
                                                    MaterialPageRoute(builder:
                                                        (BuildContext context) {
                                                  return Inicio();
                                                }));
                                              } else {
                                                _inAsyncCall = true;
                                                showDialog(
                                                    context: context,
                                                    builder: (BuildContext
                                                            context) =>
                                                        AlertDialog(
                                                            backgroundColor:
                                                                Colors.grey
                                                                    .shade400,
                                                            title: Text(
                                                              "Error",
                                                              textAlign:
                                                                  TextAlign
                                                                      .center,
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .red
                                                                      .shade800,
                                                                  fontSize: 16),
                                                            ),
                                                            content: Text(
                                                                //messagelog,
                                                                " Usuario o contraseña incorrectos o no tiene roles asignados en el sistema, consulte con el administrador",
                                                                textAlign:
                                                                    TextAlign
                                                                        .justify,
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        14))));
                                                setState(() {
                                                  usuario.clear();
                                                  contrasena.clear();
                                                  _inAsyncCall = false;
                                                });
                                              }
                                            });
                                          } else {
                                            showDialog(
                                                context: context,
                                                builder: (BuildContext
                                                        context) =>
                                                    AlertDialog(
                                                        backgroundColor: Colors
                                                            .grey.shade400,
                                                        title: Text(
                                                          "Error",
                                                          textAlign:
                                                              TextAlign.center,
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .red.shade800,
                                                              fontSize: 16),
                                                        ),
                                                        content: Text(
                                                            "Rellene todos los campos",
                                                            //"El correo o la contraseña es incorrecto o aun no esta registrado. Intente de nuevo o registrese",
                                                            textAlign: TextAlign
                                                                .justify,
                                                            style: TextStyle(
                                                                fontSize:
                                                                    14))));
                                          }
                                        },
                                        textColor: Colors.white,
                                        elevation: 5,
                                        child: Text(
                                          "Iniciar Sesión",
                                          style: TextStyle(fontSize: 14),
                                        ),
                                        color:
                                            Color.fromARGB(255, 81, 143, 167),
                                      ),
                                      TextButton(
                                          onPressed: () {
                                            Navigator.of(context).push(
                                                MaterialPageRoute(builder:
                                                    (BuildContext context) {
                                              return PaginaPrincipal();
                                            }));
                                          },
                                          child: Text("Menu Principal"))
                                    ],
                                  ))),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: height / 3,
                    )
                    /* TextButton(
                      onPressed: () {
                        Navigator.of(context).push(
                            MaterialPageRoute(builder: (BuildContext context) {
                          return Menu();
                        }));
                      },
                      child: Text("Ir a Menu"))*/
                  ],
                ),
              ),
              (_inAsyncCall)
                  ? Container(
                      decoration: BoxDecoration(
                        color: Colors.black.withOpacity(0.6),
                      ),
                      width: double.infinity,
                      height: double.infinity,
                      child: Center(child: CircularProgressIndicator()))
                  : Container()
            ],
          ))
        ],
      ),
    );
  }

  Widget userContent(icono, String titulo, pagetogo) {
    return GestureDetector(
        onTap: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (BuildContext context) {
            return pagetogo;
          }));
        },
        child: Container(
          color: Colors.white,
          padding: EdgeInsets.all(15),
          child: Row(
            children: [
              Icon(icono),
              SizedBox(
                width: 20,
              ),
              Text(titulo)
            ],
          ),
        ));
  }

  Widget text(title, size, color, font) {
    return Text(
      title,
      textAlign: TextAlign.center,
      maxLines: 2,
      style: TextStyle(
          color: color, fontSize: size, fontWeight: font, fontFamily: "Kanit"),
    );
  }

  Widget msge() {
    return Text(
      'Error!. Intenta de nuevo',
      style: TextStyle(fontSize: 12),
    );
  }
}

extension EmailValidator on String {
  bool isValidEmail() {
    return RegExp(
            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(this);
  }
}
