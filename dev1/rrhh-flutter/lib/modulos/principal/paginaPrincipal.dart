import 'package:controlmefpb/modulos/principal/paginaPermisos.dart';
import 'package:controlmefpb/modulos/principal/paginaSesionIdioma.dart';
import 'package:controlmefpb/widgets/widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PaginaPrincipal extends StatefulWidget {
  const PaginaPrincipal({Key? key}) : super(key: key);

  @override
  _PaginaPrincipalState createState() => _PaginaPrincipalState();
}

class _PaginaPrincipalState extends State<PaginaPrincipal> {
  bool val = false;
  @override
  Widget build(BuildContext context) {
    //double w = MediaQuery.of(context).size.width;
    return Theme(
        data: ThemeData(unselectedWidgetColor: Colors.white),
        child: Scaffold(
          backgroundColor: Color.fromARGB(255, 27, 87, 110),
          body: CustomScrollView(
            slivers: [
              SliverAppBar(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(50),
                          bottomRight: Radius.circular(50))),
                  expandedHeight: 80,
                  shadowColor: Colors.amber,
                  backgroundColor: Colors.white,
                  flexibleSpace: Row(children: [
                    SizedBox(
                      width: 20,
                    ),
                    Container(
                      height: 70,
                      child: Image.asset("assets/images/icono.png"),
                      margin: EdgeInsets.only(top: 40, bottom: 10),
                    ),
                    SizedBox(
                      width: 12,
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 38),
                      width: 100,
                      child: text(
                          "MINISTERIO DE ECONOMÍA Y FINANZAS PÚBLICAS",
                          8.0,
                          FontWeight.normal,
                          FontStyle.normal,
                          Colors.black),
                    )
                  ])),
              SliverToBoxAdapter(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Checkbox(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5)),
                        activeColor: Color.fromARGB(255, 211, 179, 125),
                        value: val,
                        onChanged: (value) {
                          setState(() {
                            print(value);
                            val = value!;
                          });
                        }),
                    text("Marcaciones originales", 12.0, FontWeight.normal,
                        FontStyle.normal, Colors.white),
                    SizedBox(
                      width: 10,
                    ),
                    IconButton(
                      onPressed: () {},
                      icon: Icon(
                        Icons.picture_as_pdf,
                        size: 30,
                      ),
                      color: Colors.red,
                    )
                  ],
                ),
              ),
              SliverToBoxAdapter(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  fecha("Fecha inicio"),
                  fecha("Fecha fin"),
                  buscador()
                ],
              )),
              SliverToBoxAdapter(
                child: text("Total minutos de atraso:  55 min", 12.0,
                    FontWeight.normal, FontStyle.normal, Colors.white),
              ),
              SliverList(
                delegate: SliverChildBuilderDelegate((context, item) {
                  return Card(
                    margin: EdgeInsets.only(
                        top: 10, left: 20, right: 20, bottom: 10),
                    shadowColor: Colors.black,
                    elevation: 10.0,
                    child: Container(
                      padding: EdgeInsets.all(10),
                      child: Column(children: [
                        text2("FECHA: 25/08/2021", 12.0, FontWeight.bold,
                            FontStyle.normal, Colors.black),
                        text2("Hora ingreso: 08:30", 12.0, FontWeight.bold,
                            FontStyle.normal, Colors.black),
                        text2("Hora Salida: 12:45", 12.0, FontWeight.bold,
                            FontStyle.normal, Colors.black),
                        text3(
                            "Observacion: ..................................................................",
                            12.0,
                            FontWeight.bold,
                            FontStyle.normal,
                            Colors.black),
                        text2("Hora ingreso: 08:30", 12.0, FontWeight.bold,
                            FontStyle.normal, Colors.black),
                        text2("Hora Salida: 12:45", 12.0, FontWeight.bold,
                            FontStyle.normal, Colors.black),
                        text3(
                            "Observacion: ...................................................................................................................................",
                            12.0,
                            FontWeight.bold,
                            FontStyle.normal,
                            Colors.black),
                      ]),
                    ),
                    color: Color.fromARGB(255, 81, 143, 167),
                  );
                }, childCount: 15),
              ),
            ],
          ),
          bottomNavigationBar: BottomNavigationBar(
            onTap: (item) {
              if (item == 1) {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return PaginaPermisos();
                }));
              } else if (item == 2) {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return PaginaSesionIdioma();
                }));
              }
            },
            selectedItemColor: Color.fromARGB(255, 211, 179, 125),
            backgroundColor: Color.fromARGB(255, 27, 87, 110),
            items: [
              BottomNavigationBarItem(
                  icon: Icon(Icons.app_registration), label: "Marcaciones"),
              BottomNavigationBarItem(
                  icon: Icon(Icons.home), label: "Permisos"),
              BottomNavigationBarItem(
                  icon: Icon(Icons.person_sharp), label: "Sesion e idiomas")
            ],
          ),
        ));
  }

  Widget fecha(texto) {
    return InkWell(
      borderRadius: BorderRadius.circular(50),
      splashColor: Colors.white,
      onTap: () {},
      child: Container(
        padding: EdgeInsets.all(10),
        child: Row(
          children: [
            Icon(
              Icons.calendar_today,
              color: Colors.white,
              size: 18,
            ),
            SizedBox(
              width: 10,
            ),
            text(
                texto, 12.0, FontWeight.normal, FontStyle.normal, Colors.white),
          ],
        ),
        margin: EdgeInsets.only(top: 10, bottom: 10),
        height: 35,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.black.withOpacity(0.5)),
      ),
    );
  }

  Widget buscador() {
    return InkWell(
      borderRadius: BorderRadius.circular(50),
      splashColor: Colors.white,
      onTap: () {},
      child: Container(
        padding: EdgeInsets.all(10),
        child: Row(
          children: [
            Icon(
              Icons.search,
              color: Colors.black,
              size: 20,
            ),
            SizedBox(
              width: 10,
            ),
            text("Buscar", 12.0, FontWeight.normal, FontStyle.normal,
                Colors.black),
          ],
        ),
        margin: EdgeInsets.only(top: 10, bottom: 10),
        height: 35,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Color.fromARGB(255, 211, 179, 125)),
      ),
    );
  }
}
