import 'dart:io';

import 'package:controlmefpb/modulos/principal/paginaPrincipal.dart';
import 'package:controlmefpb/modulos/principal/paginaSesionIdioma.dart';
import 'package:controlmefpb/widgets/widget.dart';
import 'package:flutter/material.dart';

class PaginaPermisos extends StatefulWidget {
  const PaginaPermisos({Key? key}) : super(key: key);

  @override
  _PaginaPermisosState createState() => _PaginaPermisosState();
}

class _PaginaPermisosState extends State<PaginaPermisos> {
  bool val = false;

  @override
  Widget build(BuildContext context) {
    double w = MediaQuery.of(context).size.width;
    return Theme(
        data: ThemeData(unselectedWidgetColor: Colors.white),
        child: Scaffold(
          backgroundColor: Color.fromARGB(255, 27, 87, 110),
          body: CustomScrollView(
            slivers: [
              SliverAppBar(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(50),
                          bottomRight: Radius.circular(50))),
                  expandedHeight: 80,
                  shadowColor: Colors.amber,
                  backgroundColor: Colors.white,
                  flexibleSpace: Row(children: [
                    SizedBox(
                      width: 20,
                    ),
                    Container(
                      height: 70,
                      child: Image.asset("assets/images/icono.png"),
                      margin: EdgeInsets.only(top: 40, bottom: 10),
                    ),
                    SizedBox(
                      width: 12,
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 38),
                      width: 100,
                      child: text(
                          "MINISTERIO DE ECONOMÍA Y FINANZAS PÚBLICAS",
                          8.0,
                          FontWeight.normal,
                          FontStyle.normal,
                          Colors.black),
                    )
                  ])),
              SliverToBoxAdapter(
                  child: Column(
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  text("TIPOS DE PERMISO", 20.0, FontWeight.normal,
                      FontStyle.normal, Colors.white),
                  SizedBox(
                    height: 10,
                  ),
                  elevatedButton("Nuevo Permiso", Colors.black, 12.0,
                      Color.fromARGB(255, 211, 179, 125))
                ],
              )),
              SliverToBoxAdapter(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [buscador(w)],
              )),
              SliverList(
                delegate: SliverChildBuilderDelegate((context, item) {
                  return Card(
                      margin: EdgeInsets.only(
                          top: 10, left: 20, right: 20, bottom: 10),
                      shadowColor: Colors.black,
                      elevation: 10.0,
                      child: Container(
                        padding: EdgeInsets.all(10),
                        child: Column(children: [
                          text2("TIPO PERMISO: LICENCIA PARTICULAR", 12.0,
                              FontWeight.bold, FontStyle.normal, Colors.black),
                          text2("FECHA REGISTRO: 05/03/2021", 12.0,
                              FontWeight.bold, FontStyle.normal, Colors.black),
                          text2("REFRIGERIO: NO", 12.0, FontWeight.bold,
                              FontStyle.normal, Colors.black),
                          text2("TIEMPO MÁXIMO (Hrs.): 2 ", 12.0,
                              FontWeight.bold, FontStyle.normal, Colors.black),
                          text2("TIEMPO MINIMO (Hrs.): 0.5 ", 12.0,
                              FontWeight.bold, FontStyle.normal, Colors.black),
                          text2("ESTADO: ACTIVO", 12.0, FontWeight.bold,
                              FontStyle.normal, Colors.black),
                          elevatedButton(
                              "Anular", Colors.black, 12.0, Colors.white)
                        ]),
                      ),
                      color: Colors.white.withOpacity(0.6)
                      //Color.fromARGB(255, 81, 143, 167),
                      );
                }, childCount: 15),
              ),
            ],
          ),
          bottomNavigationBar: BottomNavigationBar(
            currentIndex: 1,
            onTap: (item) {
              if (item == 0) {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return PaginaPrincipal();
                }));
              } else if (item == 2) {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return PaginaSesionIdioma();
                }));
              }
            },
            selectedItemColor: Color.fromARGB(255, 211, 179, 125),
            backgroundColor: Color.fromARGB(255, 27, 87, 110),
            items: [
              BottomNavigationBarItem(
                  icon: Icon(Icons.app_registration), label: "Marcaciones"),
              BottomNavigationBarItem(
                  icon: Icon(Icons.home), label: "Permisos"),
              BottomNavigationBarItem(
                  icon: Icon(Icons.person_sharp), label: "Sesion e idiomas")
            ],
          ),
        ));
  }
}

Widget fecha(texto) {
  return InkWell(
    borderRadius: BorderRadius.circular(50),
    splashColor: Colors.white,
    onTap: () {},
    child: Container(
      padding: EdgeInsets.all(10),
      child: Row(
        children: [
          Icon(
            Icons.calendar_today,
            color: Colors.white,
            size: 18,
          ),
          SizedBox(
            width: 10,
          ),
          text(texto, 12.0, FontWeight.normal, FontStyle.normal, Colors.white),
        ],
      ),
      margin: EdgeInsets.only(top: 10, bottom: 10),
      height: 35,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.black.withOpacity(0.5)),
    ),
  );
}

Widget buscador(w) {
  return InkWell(
    borderRadius: BorderRadius.circular(50),
    splashColor: Colors.white,
    onTap: () {},
    child: Container(
      width: w / 1.5,
      padding: EdgeInsets.all(10),
      child: Row(
        children: [
          Icon(
            Icons.search,
            color: Colors.white,
            size: 20,
          ),
          SizedBox(
            width: 10,
          ),
          text("Buscar por fecha", 12.0, FontWeight.normal, FontStyle.normal,
              Colors.white),
        ],
      ),
      margin: EdgeInsets.only(top: 10, bottom: 10, left: 20, right: 20),
      height: 35,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Color.fromARGB(255, 35, 33, 32)),
    ),
  );
}
