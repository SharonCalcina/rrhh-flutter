import 'package:controlmefpb/modulos/inicioSesion/paginaSesion.dart';
import 'package:controlmefpb/widgets/widget.dart';
import 'package:flutter/material.dart';

class Inicio extends StatefulWidget {
  const Inicio({Key? key}) : super(key: key);

  @override
  _InicioState createState() => _InicioState();
}

class _InicioState extends State<Inicio> {
  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: Colors.white,
        //fromARGB(255, 27, 87, 110),
        body: SingleChildScrollView(
            child: Column(
          children: [
            SafeArea(
              child: Container(
                  width: w,
                  //height: 300,
                  //color: Colors.amber,
                  child: Column(
                    children: [
                      Image.asset(
                        "assets/images/logo.png",
                        //height: ,
                        width: 300,
                      ),
                      Container(
                          margin: EdgeInsets.only(bottom: 40),
                          //padding: EdgeInsets.only(top: 20),
                          width: w / 1.5,
                          //height: 150,
                          // decoration: BoxDecoration(color: Colors.red),
                          child: text(
                              "Ministerio de Economía y Finanzas Públicas",
                              20.0,
                              FontWeight.bold,
                              FontStyle.normal,
                              Colors.black)),
                    ],
                  )),
            ),
            Container(
                //margin: EdgeInsets.only(top: 20),
                padding: EdgeInsets.only(top: 50),
                width: w,
                //height: h / 1.5,
                decoration: BoxDecoration(
                    color: Color.fromARGB(255, 27, 87, 110),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(120),
                        topRight: Radius.circular(120))),
                child: Column(
                  children: [
                    text("Bienvenido", 20.0, FontWeight.normal,
                        FontStyle.normal, Colors.white),
                    SizedBox(
                      height: 30,
                    ),
                    text("Sistema de Control de Personal", 20.0,
                        FontWeight.bold, FontStyle.normal, Colors.white),
                    SizedBox(height: h / 4),
                    button(
                        PaginaSesion(),
                        text("Continuar", 15.0, FontWeight.bold,
                            FontStyle.normal, Colors.white),
                        Color.fromARGB(255, 81, 143, 167),
                        context),
                    SizedBox(
                      height: h / 5,
                    )
                  ],
                )),
          ],
        )));
  }

  Widget button(page, text, color, context) {
    return TextButton(
        onPressed: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (BuildContext context) {
            return page;
          }));
        },
        child: text,
        style: TextButton.styleFrom(
            padding: EdgeInsets.all(15),
            backgroundColor: color,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(50)))));
  }
}
