import 'package:flutter/material.dart';

Widget button(page, text, color, context) {
  return TextButton(
      onPressed: () {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (BuildContext context) {
          return page;
        }));
      },
      child: text,
      style: TextButton.styleFrom(
          padding: EdgeInsets.all(15),
          backgroundColor: color,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(50)))));
}

Widget elevatedButton(texto, colorText, sizeText, colorButton) {
  return ElevatedButton(
      child: Text(
        texto,
        textAlign: TextAlign.left,
        style: TextStyle(
            fontFamily: "Roboto",
            fontSize: sizeText,
            fontWeight: FontWeight.bold,
            fontStyle: FontStyle.normal,
            color: colorText),
      ),
      style: TextButton.styleFrom(
        elevation: 5.0,
        shadowColor: Colors.white,
        backgroundColor: colorButton,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      ),
      onPressed: () {});
}

Widget text2(text, size, w, style, color) {
  return Align(
      alignment: Alignment.centerLeft,
      child: Text(
        text,
        textAlign: TextAlign.left,
        style: TextStyle(
            fontFamily: "Roboto",
            fontSize: size,
            fontWeight: w,
            fontStyle: style,
            color: color),
      ));
}

Widget text3(text, size, w, style, color) {
  return Align(
      alignment: Alignment.centerLeft,
      child: Text(
        text,
        maxLines: 2,
        textAlign: TextAlign.left,
        style: TextStyle(
            fontFamily: "Roboto",
            fontSize: size,
            fontWeight: w,
            fontStyle: style,
            color: color),
      ));
}

Widget text(text, size, w, style, color) {
  return Text(
    text,
    textAlign: TextAlign.center,
    style: TextStyle(
        fontFamily: "Roboto",
        fontSize: size,
        fontWeight: w,
        fontStyle: style,
        color: color),
  );
}
